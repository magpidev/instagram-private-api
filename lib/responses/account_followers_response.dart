import 'package:json_annotation/json_annotation.dart';

part 'account_followers_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class AccountFollowersResponse {
  dynamic nextMaxId;
  List<UsersInfoResponseUser> users;
  String status;
  AccountFollowersResponse();
  factory AccountFollowersResponse.fromJson(Map<String, dynamic> json) => _$AccountFollowersResponseFromJson(json);
  Map<String, dynamic> toJson() => _$AccountFollowersResponseToJson(this);
}


@JsonSerializable(fieldRename: FieldRename.snake)
class UsersInfoResponseUser {
  int pk;
  String username;
  String fullName;
  bool isPrivate;
  String profilePicUrl;
  bool isVerified;
  bool hasAnonymousProfilePicture;
  UsersInfoResponseUser();
  factory UsersInfoResponseUser.fromJson(Map<String, dynamic> json) => _$UsersInfoResponseUserFromJson(json);
  Map<String, dynamic> toJson() => _$UsersInfoResponseUserToJson(this);
}
