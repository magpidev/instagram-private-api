// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reel_media_viewer_feed_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReelMediaViewerFeedResponse _$ReelMediaViewerFeedResponseFromJson(
    Map<String, dynamic> json) {
  return ReelMediaViewerFeedResponse()
    ..nextMaxId = json['next_max_id']
    ..users = (json['users'] as List)
        ?.map((e) => e == null
            ? null
            : FeedUserStoryResponseReelUser.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..status = json['status'] as String;
}

Map<String, dynamic> _$ReelMediaViewerFeedResponseToJson(
        ReelMediaViewerFeedResponse instance) =>
    <String, dynamic>{
      'next_max_id': instance.nextMaxId,
      'users': instance.users,
      'status': instance.status,
    };

FeedUserStoryResponseReelUser _$FeedUserStoryResponseReelUserFromJson(
    Map<String, dynamic> json) {
  return FeedUserStoryResponseReelUser()
    ..pk = json['pk'] as int
    ..username = json['username'] as String
    ..fullName = json['full_name'] as String
    ..isPrivate = json['is_private'] as bool
    ..profilePicUrl = json['profile_pic_url'] as String
    ..profilePicId = json['profile_pic_id'] as String
    ..isVerified = json['is_verified'] as bool;
}

Map<String, dynamic> _$FeedUserStoryResponseReelUserToJson(
        FeedUserStoryResponseReelUser instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'username': instance.username,
      'full_name': instance.fullName,
      'is_private': instance.isPrivate,
      'profile_pic_url': instance.profilePicUrl,
      'profile_pic_id': instance.profilePicId,
      'is_verified': instance.isVerified,
    };
