// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_followers_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountFollowersResponse _$AccountFollowersResponseFromJson(
    Map<String, dynamic> json) {
  return AccountFollowersResponse()
    ..nextMaxId = json['next_max_id']
    ..users = (json['users'] as List)
        ?.map((e) => e == null
            ? null
            : UsersInfoResponseUser.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..status = json['status'] as String;
}

Map<String, dynamic> _$AccountFollowersResponseToJson(
        AccountFollowersResponse instance) =>
    <String, dynamic>{
      'next_max_id': instance.nextMaxId,
      'users': instance.users,
      'status': instance.status,
    };

UsersInfoResponseUser _$UsersInfoResponseUserFromJson(
    Map<String, dynamic> json) {
  return UsersInfoResponseUser()
    ..pk = json['pk'] as int
    ..username = json['username'] as String
    ..fullName = json['full_name'] as String
    ..isPrivate = json['is_private'] as bool
    ..profilePicUrl = json['profile_pic_url'] as String
    ..isVerified = json['is_verified'] as bool
    ..hasAnonymousProfilePicture =
        json['has_anonymous_profile_picture'] as bool;
}

Map<String, dynamic> _$UsersInfoResponseUserToJson(
        UsersInfoResponseUser instance) =>
    <String, dynamic>{
      'pk': instance.pk,
      'username': instance.username,
      'full_name': instance.fullName,
      'is_private': instance.isPrivate,
      'profile_pic_url': instance.profilePicUrl,
      'is_verified': instance.isVerified,
      'has_anonymous_profile_picture': instance.hasAnonymousProfilePicture,
    };
