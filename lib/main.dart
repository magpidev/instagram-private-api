import 'dart:async';
import 'dart:convert';
import 'dart:io';


import 'package:flutter/material.dart';
import 'package:flutter_user_agent/flutter_user_agent.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart' as dioCookieManager;
import 'package:cookie_jar/cookie_jar.dart';
import 'package:http/http.dart' as http;
import 'package:interview1/classes/AccountFollowersResponse.dart';
import 'package:interview1/responses/account_followers_response.dart';
import 'package:interview1/responses/info_response.dart';
import 'package:interview1/responses/reel_media_viewer_feed_response.dart';
import 'package:interview1/responses/search_response.dart';
import 'package:interview1/responses/user_story_response.dart';


import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  StreamSubscription<String> _onStateChanged;


  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();

  }

  String getId(String cookies) {
    final startIndex = cookies.indexOf("ds_user_id=");
    final endIndex = cookies.indexOf(";", startIndex + "ds_user_id=".length);
    return cookies.substring(startIndex + "ds_user_id=".length, endIndex);
  }

  Future<AccountFollowersResponse> getFollowers(String id, String cookies, [String nextMaxId = ""]) async {
    Uri url = Uri.parse("https://i.instagram.com/api/v1/friendships/$id/followers/?max_id=$nextMaxId");
    String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)";
    var response = await http.get(
      url,
      headers: {HttpHeaders.userAgentHeader: ua, HttpHeaders.cookieHeader: cookies},
    );
    return AccountFollowersResponse.fromJson(json.decode(response.body));
  }

  Future<AccountFollowersResponse> getFollowing(String id, String cookies, [String nextMaxId = ""]) async {
    Uri url = Uri.parse("https://i.instagram.com/api/v1/friendships/$id/following/?max_id=$nextMaxId");
    String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)";
    var response = await http.get(
      url,
      headers: {HttpHeaders.userAgentHeader: ua, HttpHeaders.cookieHeader: cookies},
    );
    return AccountFollowersResponse.fromJson(json.decode(response.body));
  }

  Future<FeedUserStoryResponse> getStory(String id, String cookies) async {
    Uri url = Uri.parse("https://i.instagram.com/api/v1/feed/user/$id/story/");
    String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)";
    var response = await http.get(
      url,
      headers: {HttpHeaders.userAgentHeader: ua, HttpHeaders.cookieHeader: cookies},
    );
    return FeedUserStoryResponse.fromJson(json.decode(response.body));
  }

  Future<FeedUserStoryResponse> getMyStory(String cookies) async {
    return getStory(getId(cookies), cookies);
  }

  Future<UsersSearchResponse> searchUser(String query, String cookies) async {
    Uri url = Uri.parse("https://i.instagram.com/api/v1/users/search/?q=$query");
    String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)";
    var response = await http.get(
      url,
      headers: {HttpHeaders.userAgentHeader: ua, HttpHeaders.cookieHeader: cookies},
    );
    return UsersSearchResponse.fromJson(json.decode(response.body));
  }

  Future<UsersInfoResponse> getUser(String userPk, String cookies, [String nextMaxId = ""]) async {
    Uri url = Uri.parse("https://i.instagram.com/api/v1/users/$userPk/info/");
    String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)";
    var response = await http.get(
      url,
      headers: {HttpHeaders.userAgentHeader: ua, HttpHeaders.cookieHeader: cookies},
    );
    return UsersInfoResponse.fromJson(json.decode(response.body));
  }

  Future<AccountFollowersResponse> getMyFollowers(String cookies, [String nextMaxId = ""]) async {
    return nextMaxId != "" ? getFollowers(getId(cookies), cookies, nextMaxId) : getFollowers(getId(cookies), cookies);
  }

  Future<ReelMediaViewerFeedResponse> getStoryViewers(String mediaId, String cookies, [String nextMaxId]) async {
    Uri url = Uri.parse("https://i.instagram.com/api/v1/media/$mediaId/list_reel_media_viewer/?max_id=$nextMaxId");
    String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)";
    var response = await http.get(
      url,
      headers: {HttpHeaders.userAgentHeader: ua, HttpHeaders.cookieHeader: cookies},
    );
    return ReelMediaViewerFeedResponse.fromJson(json.decode(response.body));
  }

  Future<void> getCookies() async {
    final cookieManager = WebviewCookieManager();
    var cookieString = "";
    final gotCookies = await cookieManager.getCookies('https://instagram.com');
    for (var item in gotCookies) {
      cookieString += "${item.name}=${item.value};";
    }
    try {
      AccountFollowersResponse followers = await getFollowers("427553890", cookieString);
      AccountFollowersResponse myfollowers = await getMyFollowers(cookieString);
      //var story = await getStory("3640709640", cookieString);
    } catch (e) {
      print(e);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
              child: Text("Login"),
              onPressed: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => WebView(
                          onPageFinished: (String url) {
                            if(url == "https://www.instagram.com/") {
                              // Already logged in & valid
                              //Navigator.pop(context);
                              print("Logged in");
                            } else {
                              // Not logged in yet / invalid
                              print("Not logged in");
                            }
                          },
                          javascriptMode: JavascriptMode.unrestricted,
                          initialUrl: 'https://www.instagram.com/accounts/login/',
                        )));
              },
            ),
            ElevatedButton(
              child: Text("Get Cookies"),
              onPressed: (){
                getCookies();
              },
            ),
          ]
        ),
      ),
    );
  }
}
