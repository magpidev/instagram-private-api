class AccountFollower {
  /*
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  latest_reel_media?: number;
   */
  final int pk;
  final String username;
  final String fullName;
  final bool isPrivate;
  final String profilePicUrl;
  final bool isVerified;
  final bool hasAnonymousProfilePicture;


  AccountFollower({this.pk, this.username, this.fullName, this.isPrivate, this.profilePicUrl, this.isVerified, this.hasAnonymousProfilePicture});

  factory AccountFollower.fromJson(Map<String, dynamic> json) {
    return AccountFollower(
      pk: json['pk'],
      username: json['username'],
      fullName: json['full_name'],
      isPrivate: json['is_private'],
      profilePicUrl: json['profile_pic_url'],
      isVerified: json['is_verified'],
      hasAnonymousProfilePicture: json['has_anonymous_profile_picture'],
    );
  }
}
